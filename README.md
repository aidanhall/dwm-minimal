dwm - Minimal Edition
=====================

My main aim for this version of dwm is to make it as minimal as possible.
I will do this by only bringing over my most frequently used features
from my previous dwm build.

# Other goals include:
* Making a separate branch for all non-trivial patches to reduce the amount of manual patching I need to do.
* Creating a diff file for every major feature I add, including tag labels and window follow, so these can be shared on the suckless website.
* Keeping the repository clean: dwm.c.orig.orig isn't helping anybody.
* Fixing any bugs that may be causing my previous build to crash.

%% vim: ft=markdown
